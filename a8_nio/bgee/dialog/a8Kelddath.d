APPEND KELDDA

  IF ~~ THEN BEGIN a8sper03a
    SAY @5051
      +~
        PartyHasItem("scrl5g")
        PartyGoldGT(999)
      ~+ @5052 GOTO a8sper03b
      ++ @5053 GOTO 0
  END

  IF ~~ THEN BEGIN a8sper03b
    SAY @5054 IF ~~ THEN DO ~
      TakePartyGold(1000)
      TakePartyItem("sper03")
      TakePartyItemNum("scrl5g", 1)
      DestroyItem("sper03")
      SetGlobal("a8sper03", "GLOBAL", 1)
      SetGlobalTimer("a8sper03time", "GLOBAL", ONE_DAY)
    ~ EXIT
  END

  IF ~~ THEN BEGIN a8sper03c
    SAY @5056 IF ~~ THEN DO ~~ GOTO 0
  END

  IF WEIGHT #0 ~
      GlobalTimerExpired("a8sper03time", "GLOBAL")
      Global("a8sper03", "GLOBAL", 1)
    ~ THEN BEGIN a8sper03d
    SAY @5057 IF ~~ THEN DO ~
      SetGlobal("a8sper03", "GLOBAL", 2)
      GiveItemCreate("a8sper03", LastTalkedToBy, 0, 0, 0)
    ~ EXIT
  END

END

EXTEND_BOTTOM KELDDA 0
  +~
    PartyHasItem("SPER03")
    Global("a8sper03", "GLOBAL", 0)
  ~+ @5050 GOTO a8sper03a
  +~
    Global("a8sper03", "GLOBAL", 1)
    GlobalTimerNotExpired("a8sper03time", "GLOBAL")
  ~+ @5055 GOTO a8sper03c
END