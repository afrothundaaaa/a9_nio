BEGIN A8SIDWHI

IF WEIGHT #100 ~True()~ THEN BEGIN 100
  SAY @5952
    ++  @5953 GOTO 101
    ++  @5954 EXIT
    +~
      GlobalTimerNotExpired("a8ring05time", "GLOBAL")
      Global("a8ring05", "GLOBAL", 1)
    ~+  @5960 GOTO 104
    +~
      GlobalTimerExpired("a8ring05time", "GLOBAL")
      Global("a8ring05", "GLOBAL", 1)
    ~+  @5960 GOTO 105
END

IF ~~ THEN BEGIN 101
  SAY @5955
    +~
      PartyHasItem("ring05")
      Global("a8ring05", "GLOBAL", 0)
    ~+  @5957 GOTO 102
    ++  @5958 EXIT
END

IF ~~ THEN BEGIN 102
  SAY @5956
    +~
      PartyGoldGT(2499)
      PartyHasItem("scrl71")
    ~+  @5957 GOTO 103
    +~~+  @5958 EXIT
END

IF ~~ THEN BEGIN 103
  SAY @5959 IF ~~ THEN DO ~
    TakePartyItem("ring05")
    TakePartyItemNum("scrl71", 1)
    DestroyItem("ring05")
    DestroyItem("scrl71")
    SetGlobal("a8ring05", "GLOBAL", 1)
    SetGlobalTimer("a8ring05time", "GLOBAL", TWO_DAYS)
  ~ EXIT
END

IF ~~ THEN BEGIN 104
  SAY @5961 IF ~~ THEN DO ~~ EXIT
END

IF ~~ THEN BEGIN 105
  SAY @5962 IF ~~ THEN DO ~
    SetGlobal("a8ring05", "GLOBAL", 2)
    GiveItemCreate("a8ring05", LastTalkedToBy, 5, 0, 0)
  ~ EXIT
END