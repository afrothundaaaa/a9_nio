APPEND THALAN
  IF ~~ THEN BEGIN a8ThalanDP1
    SAY @5119
      +~
        PartyHasItem("scrl68")
        PartyGoldGT(999)
      ~+ @5120 GOTO a8ThalanDP2
      ++ @5121 DO ~AddJournalEntry(@5103, QUEST)~ GOTO 1
  END

  IF ~~ THEN BEGIN a8ThalanDP2
    SAY @5114 IF ~~ THEN DO ~
      TakePartyGold(1000)
      TakePartyItem("a8swdp1a")
      DestroyItem("a8swdp1a")
      TakePartyItem("scrl68")
      DestroyItem("scrl68")
      SetGlobal("a8SWDP1", "GLOBAL", 3)
      SetGlobalTimer("a8swdp1t2", "GLOBAL", ONE_DAY)
      AddJournalEntry(@5104, QUEST)
    ~ EXIT
  END

  IF ~~ THEN BEGIN a8ThalanDP3
    SAY @5123 IF ~~ THEN DO ~~ GOTO 1
  END

  IF WEIGHT #0 ~
      Global("a8SWDP1", "GLOBAL", 3)
      GlobalTimerExpired("a8swdp1t2", "GLOBAL")
    ~ THEN BEGIN a8ThalanDP4
    SAY @5124 IF ~~ THEN DO ~
      GiveItemCreate("a8swdp1b", LastTalkedToBy, 3, 0, 0)
      SetGlobal("a8SWDP1", "GLOBAL", 4)
      AddJournalEntry(@5105, QUEST_DONE)
    ~ EXIT
  END
END

EXTEND_BOTTOM THALAN 1
  +~
    Global("a8SWDP1", "GLOBAL", 2)
    PartyHasItem("a8swdp1a")
  ~+  @5118 GOTO a8ThalanDP1
  +~
    Global("a8SWDP1","GLOBAL", 3)
    GlobalTimerNotExpired("a8swdp1t2", "GLOBAL")
  ~+ @5122 GOTO a8ThalanDP3
END