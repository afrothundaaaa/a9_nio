APPEND ALVAHE
  IF ~~ THEN BEGIN a8clcktt1
    SAY @5154
      +~PartyGoldGT(1999)~+ @5155 GOTO a8clcktt2
      ++ @5156 DO ~AddJournalEntry(@5150, QUEST)~ EXIT
  END

  IF ~~ THEN BEGIN a8clcktt2
    SAY @5157 IF ~~ THEN DO ~
      TakePartyGold(2000)
      TakePartyItem("clck06")
      DestroyItem("clck06")
      SetGlobal("a8clcktt", "GLOBAL", 1)
      SetGlobalTimer("a8clcktttime", "GLOBAL", THREE_DAYS)
      AddJournalEntry(@5151, QUEST)
    ~ EXIT
  END

  IF ~~ THEN BEGIN a8clcktt3
    SAY @5159 IF ~~ THEN DO ~~ GOTO 0
  END

  IF WEIGHT #0 ~
    Global("a8clcktt", "GLOBAL", 1)
    GlobalTimerExpired("a8clcktttime", "GLOBAL")
  ~ THEN BEGIN a8clcktt4
    SAY @5160 IF ~~ THEN DO ~
      GiveItemCreate("a8clcktt", LastTalkedToBy, 0, 0, 0)
      SetGlobal("a8clcktt", "GLOBAL", 2)
      AddJournalEntry(@5152, QUEST_DONE)
    ~ EXIT
  END
END

EXTEND_BOTTOM ALVAHE 0
  +~
    Global("a8clcktt","GLOBAL",0)
    PartyHasItem("clck06")
  ~+ @5153 GOTO a8clcktt1
  +~
    Global("a8clcktt", "GLOBAL", 1)
    GlobalTimerNotExpired("a8clcktttime", "GLOBAL")
  ~+ @5158 GOTO a8clcktt3
  +~
    Global("a8clcktt", "GLOBAL", 1)
    GlobalTimerExpired("a8clcktttime", "GLOBAL")
  ~+ @5158 GOTO a8clcktt4
END