APPEND TAEROM

  IF ~~ THEN BEGIN a8TaeromHide1
    SAY @5003
      +~PartyGoldGT(1499)~+ @5004 GOTO a8TaeromHide2
      ++ @5005 DO ~AddJournalEntry(@5000, QUEST)~ GOTO 14
  END

  IF ~~ THEN BEGIN a8TaeromHide2
    SAY @5006 IF ~~ THEN DO ~
      TakePartyGold(1500)
      TakePartyItem("misc01")
      DestroyItem("misc01")
      SetGlobal("a8TaeromHide1", "GLOBAL", 1)
      SetGlobalTimer("a8Taerom1", "GLOBAL", THREE_DAYS)
      AddJournalEntry(@5001, QUEST)
    ~ EXIT
  END

  IF ~~ THEN BEGIN a8TaeromHide3
    SAY @5007 IF ~~ THEN DO ~~ GOTO 14
  END

  IF WEIGHT #0 ~
      Global("a8TaeromHide1", "GLOBAL", 1)
      GlobalTimerExpired("a8Taerom1", "GLOBAL")
    ~ THEN BEGIN a8TaeromHideFinished
    SAY @5008 IF ~~ THEN DO ~
      GiveItemCreate("a8hidejs", LastTalkedToBy, 2, 3, 0)
      SetGlobal("a8TaeromHide1", "GLOBAL", 2)
      AddJournalEntry(@5002, QUEST_DONE)
    ~ EXIT
  END

END

EXTEND_BOTTOM TAEROM 14
  +~
    PartyHasItem("MISC01")
    Global("a8TaeromHide1", "GLOBAL", 0)
  ~+ @5009 GOTO a8TaeromHide1
  +~
    Global("a8TaeromHide1","GLOBAL", 1)
    GlobalTimerNotExpired("a8Taerom1", "GLOBAL")
  ~+ @5010 GOTO a8TaeromHide3
END